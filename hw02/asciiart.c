#include <stdio.h>
#include <stdlib.h>

#include "asciifont.h"

#define UNUSED(var) ((void) (var))
#define MIN(x, y) (((x) < (y)) ? (x) : (y))

void print_ascii(int asciiValue, int row);


int main(int argc, char *argv[])
{
    if (argc != 2) {
        fprintf(stderr, "Error: Incorect number of input parameters!\n");
        return 1;
    }
    if (atoi(argv[1]) == 0) {
        fprintf(stderr, "Error: Parameter is not a number!\n");
        return 1;
    }
    if (atoi(argv[1]) < 8) {
        fprintf(stderr, "Error: Parameter is smaller than 8!\n");
        return 1;
    }

    char userInput[101] = { 0 };
    int numOfChars = 0;
    int charsOnLine = atoi(argv[1]) / 8;


    fgets(userInput, 101, stdin);


    for (int i = 0; i < 100; ++i) {
        int numASCIIValue = (int) userInput[i];
        if (numASCIIValue < 0 || numASCIIValue > 127) {
            fprintf(stderr, "Error: Input contains a nonASCII character!\n");
            return 1;
        }
        if (userInput[i] == '\n') {
            numOfChars = i;
            break;
        }
    }
    if (numOfChars == 0) {
        numOfChars = 100;
    }

    int asciiLines = numOfChars / charsOnLine;

    if (numOfChars > 0) {
        for (int asciiBlock = 0; asciiBlock <= asciiLines; ++asciiBlock) {
            for (int row = 0; row < 8; ++row) {
                for (int character = 0; character < MIN(charsOnLine, numOfChars); ++character) {
                    print_ascii((int) userInput[character + (asciiBlock * charsOnLine)], row);
                }
                if (numOfChars > 0) {
                    printf("\n");
                }
            }
            numOfChars -= charsOnLine;
        }
    }

    return 0;
}

void print_ascii(int asciiValue, int row)
{
    for (int i = 0; i < 8; ++i) {
        if (FONT_DATA[asciiValue][row] & (128 >> i)) {
            printf("#");
        } else {
            printf(" ");
        }
    }
}




