#ifndef UTFCONVERT_H
#define UTFCONVERT_H

unsigned int utf8ToUnicode (unsigned int hexNumber);
unsigned int unicodeToUtf16 (unsigned int unicode);
unsigned int getBitsFromNumber (int start, int numOfBites, unsigned int number);
int getBitCount(unsigned int number);
void printOutput (unsigned int Utf16HexValue);

#endif
