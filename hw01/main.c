#include "utfconvert.h"
#include <stdio.h>
#include <stdlib.h>

int main(void)
{
    /*
    char inputString[64];
    fgets(inputString, sizeof inputString, stdin);


    if(inputString[0] == '\n') {
        printf("Invalid input!\n");
        printf("prazdný vstup\n");
        return 1;
    }

    if(inputString[0] == '-') {
        printf("Invalid input!\n");
        printf("zaporne cislo\n");
        return 1;
    }

    if (inputString[0] < '0' || inputString[0] > '9'){
        printf("Invalid input!\n");
        printf("neni cislo\n");
        return 1;
    }

    printf("%s",inputString);

    float scanedNumber = -1.0;

    if (scanf("%f", &scanedNumber) != 1) {
        printf("Invalid input!1\n");
        return 1;
    }

    if(scanedNumber < 0){
        printf("Invalid input!2\n");
        return 1;
    }

    if (scanedNumber - (int)scanedNumber != 0){
        printf("Invalid input!5\n");
        return 1;
    }

    numOfChars = (int) scanedNumber;
    printf("tvoje čislo:%d", numOfChars);
    */


    char hexNumber[9] = {'z','z','z','z','z','z','z','z','z'};
    char firstNum[16] = {'z','z','z','z','z','z','z','z','z','z','z','z','z','z','z','z'};
    int numOfChars = 0;
    int charactersRead = 0;
    int savedHexValues = 0;
    int hexDigitsSaved = 0;
    int lastChar = 3;
    char scannedChar = 'z';

    while(scanf("%c", &scannedChar) == 1){
        if(scannedChar >= '0' && scannedChar <= '9'){
            //printf("Našel jsem číslo\n");
            //numOfChars += (scannedChar - '0') * i;
            firstNum[charactersRead] = scannedChar;
            charactersRead++;
        } else if (((scannedChar >= 'a' && scannedChar <= 'f') || (scannedChar >= 'A' && scannedChar <= 'F') || (scannedChar >= '0' && scannedChar <= '9')) && charactersRead > 0) {
            hexNumber[0] = scannedChar;
            hexDigitsSaved = 1;
            lastChar = 1;
            break;
        } else if ((scannedChar == ' ' || scannedChar == '\n') && charactersRead > 0) {
            lastChar = 2;
            break;
        } else {
            printf("Invalid input! hey\n");
            return 1;
        }
    }

    numOfChars = (int)strtol(firstNum, NULL, 10);
    unsigned int array[numOfChars];


    while(scanf("%c", &scannedChar) == 1){
        if (scannedChar == ' ') {
            //printf("Našel jsem bilý znak\n");
            if(lastChar == 1) {
                array[savedHexValues] = (unsigned int)strtol(hexNumber, NULL, 16);
                savedHexValues++;
            }
            lastChar = 2;
            hexDigitsSaved = 0;

        } else if (((scannedChar >= 'a' && scannedChar <= 'f') || (scannedChar >= 'A' && scannedChar <= 'F') || (scannedChar >= '0' && scannedChar <= '9')) && charactersRead > 0) {
            //printf("Našel jsem digit z HEX\n");
            hexNumber[hexDigitsSaved] = scannedChar;
            hexDigitsSaved++;
            lastChar = 1;

        } else if(scannedChar == '\n' || scannedChar == '\0'){
            if(lastChar == 1) {
                array[savedHexValues] = (unsigned int)strtol(hexNumber, NULL, 16);
                savedHexValues++;
            }
            if (savedHexValues == numOfChars) {
                break;
            } else {
                printf("Invalid input!\n");
                return 1;
            }
        }else {
            printf("Invalid input!\n");
            return 1;
        }
    }

    //printf("Celkem čísel:%d\n", numOfChars);
/*
    for (int i = 0; i < numOfChars; ++i) {
        //printf("skenuji pro HEX hodnotu...");
        if (scanf("%x", &array[i]) != 1) {
            printf("Invalid input!\n");
            return 1;
        }

    }
*/
    for (int i = 0; i < numOfChars; ++i) {
        int bitLength = getBitCount(array[i]);
        if(((bitLength % 8 != 0) != (bitLength < 8)) && bitLength != 0){
            printf("Invalid input!\n");
            return 1;
        }
        printOutput(unicodeToUtf16(utf8ToUnicode(array[i])));
    }

}

