#include "utfconvert.h"
#include <stdio.h>
#include <stdlib.h>

int getBitCount(unsigned int number){
    int count = 0;
    while (number)
    {
         count++;
         number >>= 1;
     }
     return count;
}

unsigned int utf8ToUnicode (unsigned int hexNumber){
    int j = 0;
    unsigned int new_number = 0;
    int numberOfBits = getBitCount(hexNumber);
    if(numberOfBits > 8){
        for (int i = 0; i < numberOfBits-(int)(numberOfBits/8)-1; ++i) {
            if (!((i%8 == 7) || (i%8 == 6))){
                if (hexNumber & (1 << i)){
                    //printf("%s\n", "1");
                    new_number |= 1 << j;
                } else {
                   //printf("%s\n", "0");
                }
                j++;
            }
        }
    } else {
        new_number = hexNumber;
    }
    return new_number;
}

unsigned int unicodeToUtf16 (unsigned int unicode){
    unsigned int Utf16 =  unicode;

    if(unicode >= 0x10000){

        Utf16 -= 0x10000;

        unsigned int bottomBites = getBitsFromNumber (0, 10, Utf16) + 0xDC00;
        unsigned int upperBites = getBitsFromNumber (10, 10, Utf16) + 0xD800;

        Utf16 = (upperBites<<16) | bottomBites;
        //printf("%X, %X\n", bottomBites, upperBites);
        //printf("%X\n", new_number);
    }
    return Utf16;
}

unsigned int getBitsFromNumber (int start, int numOfBites, unsigned int number){
    int j = 0;
    unsigned int newNumber = 0;
    for (int i = start; i < start + numOfBites; ++i) {
                if (number & (1 << i)){
                    newNumber |= 1 << j;
                }
                j++;
            }
    return newNumber;
}

void printOutput (unsigned int Utf16HexValue){
    int numOfRepets = 2;
    if (getBitCount(Utf16HexValue)>16){
        numOfRepets = 4;
    }
    for (int i = numOfRepets-1; i >= 0; --i) {
        printf("%03d", getBitsFromNumber(i*8, 8, Utf16HexValue));
    }
    printf(" 0x%04X", Utf16HexValue);
    printf("\n");
}


